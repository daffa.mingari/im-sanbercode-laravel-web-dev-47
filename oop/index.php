<?php
    require ('animal.php');
    require ('frog.php');
    require ('ape.php');

$object = new Animal("shaun");

echo "Name : $object->name <br>";
echo "legs : $object->legs <br>";
echo "cold blooded : $object->cold_blooded <br> <br>";

$object2 = new Frog("buduk");

echo "Name : $object2->name <br>";
echo "legs : $object2->legs <br>";
echo "cold blooded : $object2->cold_blooded <br>";
echo "Hop Hop <br> <br>" . $object2->jump();

$object3 = new Ape("kera sakti");

echo "Name : $object3->name <br>";
echo "legs : $object3->legs <br>";
echo "cold blooded : $object3->cold_blooded <br>";
echo "Auooo" . $object3->yell();

?>